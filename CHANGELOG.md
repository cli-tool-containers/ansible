# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

<!--
## [Unreleased]
-->

## [Unreleased]
### Changed
- Renamed `Dockerfile-ansible` to `Dockerfile`
- Slight changes to `Dockerfile`
- Updated `README.md`

## [2.7.0] - 2019-04-29
### Added
- README.md is created
- CHANGELOG.md is created
- LICENSE.md is added

### Changed
- Updated Dockerfiles for Ansible and Ansible-playbook

<!--
## [0.0.0] - 2017-06-20
### Added
### Changed
### Fixed
### Removed
-->

<!--
[Unreleased]: https://github.com/olivierlacan/keep-a-changelog/compare/v1.0.0...HEAD
-->
[2.7.0]: https://gitlab.com/cli-tool-containers/terraform/tags/2.7.0

