# Ansible Container

This repository contains two minimalistic images to use the `ansible` and `ansible-playbook` CLI tools inside a container. 

## **TL;DR**
Clone this repository and run:
```bash
docker build -t ansible . && docker run --rm -t --entrypoint=ansible ansible --version
```

## How-To

A container running Ansible. Instead of installing and maintaining a CLI tool on you environment, you can use this container to execute your Ansible scripts and playbooks.

### Build

Build the docker container for `ansible` and `ansible-playboook`:

```bash
docker build -t ansible:2.7.0 .
```

### Run

Then use the container image like this:
```bash
docker run --rm -t --entrypoint=ansible ansible:2.7.0 <your-ansible-command>
```

Note: the `-t` flag enables colored output from the container to your shell.

Or if you like to run a playbook, using `ansible-playbook` run:
```bash
docker run --rm -t --entrypoint=ansible-playbook ansible:2.7.0 <your-ansible-command>
```

As an example, see the `ansible --version` result by running:
```bash
docker run --rm -t --entrypoint=ansible ansible:2.7.0 --version
```

Additionally you can give the container access to the current directory:
```bash
docker run --rm -t --entrypoint=ansible -v ~/.ssh:/root/.ssh -v /etc/hosts:/etc/hosts -v $(pwd):/current-directory -w="/current-directory" ansible:2.7.0 <your-ansible-command>
```

### Tips&Tricks

You can add the following line to your `.bash_profile`:
```bash
alias ansible="docker run --rm -t --entrypoint=ansible -v ~/.ssh:/root/.ssh -v /etc/hosts:/etc/hosts -v $(pwd):/current-directory -w="/current-directory" ansible:2.7.0"
```

## Versioning

The versioning of Ansible is advised to use in the tags of the images. For the versions available, see the [tags on this repository](https://gitlab.com/cli-tool-containers/ansible/tags) or the [CHANGELOG.md](CHANGELOG.md).

## Authors

* **Michiel Bakker** - *Owner/maintainer* - [GitLab profile](https://gitlab.com/mvbakker)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* [This README template](https://gist.github.com/PurpleBooth/109311bb0361f32d87a2) by PurpleBooth
