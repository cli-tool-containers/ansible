FROM alpine:3.9

ENV ANSIBLE_GATHERING smart
ENV ANSIBLE_HOST_KEY_CHECKING false
ENV ANSIBLE_RETRY_FILES_ENABLED false
ENV ANSIBLE_ROLES_PATH /ansible/playbooks/roles:/current-dir/roles:/current-dir/external_roles
ENV ANSIBLE_SSH_PIPELINING True
ENV PATH /ansible/bin:$PATH
ENV PYTHONPATH /ansible/lib

RUN apk add \
    curl \
    openssh-client \
    openssl \
    ansible=2.7.0-r1 && \
    rm -rf /var/cache/apk/* && \
    mkdir -p /etc/ansible && \
    echo 'localhost' > /etc/ansible/hosts && \
    echo 'host.docker.internal' >> /etc/ansible/hosts

CMD ["/bin/sh"]
